/**
 * Disable compilation warning from external dependency to have clean build output
 * -Wmismatched-tags
 */
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wmismatched-tags"
#include "_json.hpp"
#pragma GCC diagnostic pop