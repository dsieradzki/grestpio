# gRESTpIO
Software for managing IoT device.

Goals:
 - Changing state of GPIO by REST request
 - Sending request to external server when GPIO change state
 - Managing software by web application
  
## Overview 
![gRESTpIO Overview](documentation/gRESTpIO.jpg)
# Build
## x86 Unix OS
 - go to scripts directory
 - run build_host_x86.sh
 - dist/ directory contains built application
 - to run ./grestpio
# Platforms
 - Unix machine (with mocked GPIO)
 - Raspberry PI
 - Onion Omega 2
## Screenshots
 ![gRESTpIO Edit Rule](documentation/edit_rule.png)
 ![gRESTpIO Logs](documentation/logs.png)
 ![gRESTpIO Settings](documentation/settings.png)
 ![gRESTpIO Example](documentation/example.png)
# Used technologies
 - C++11
 - Angular 6
