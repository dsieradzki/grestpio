#!/usr/bin/env bash

cd ..
rm -rf ./dist
rm -rf ./build

mkdir ./dist
mkdir ./dist/www
mkdir ./dist/conf
mkdir ./dist/logs

#Build Backend
docker run -v $(pwd):/sources -it dsieradzki/raspberry-compiler:1.0.0 /bin/bash -c "cd /sources;mkdir build;cd build;cmake -DGRESTPIO_TARGET_PLATFORM=RASPBERRY_PI ..;make"

#Build Frontend
docker run -v $(pwd)/main/resources/webapp:/sources -it node:10.15.2-jessie /bin/sh -c "cd /sources;/usr/local/bin/npm install;/usr/local/bin/npm run-script build"

#Copying artifacts
cp $(pwd)/main/resources/webapp/dist/grestpio/* ./dist/www
cp $(pwd)/build/grestpio ./dist/