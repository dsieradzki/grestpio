#!/usr/bin/env bash

cd ..
mkdir build
cd build
cmake -DGRESTPIO_TARGET_PLATFORM=x86 --target grestpio ..
make

cd ..
mkdir dist
mkdir dist/www

cp build/grestpio dist/grestpio


cd main/resources/webapp
npm install
npm run-script build
cp dist/grestpio/* ../../../dist/www

#TODO CLEAN