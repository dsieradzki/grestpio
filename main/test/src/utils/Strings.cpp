#include "../../lib/catch.hpp"
#include <utils/String.hpp>

#include <string>

TEST_CASE("Split string", "[Strings]") {
	std::string text = "one/two/three/four/";


	std::vector<std::string> result = grestpio::string::split(text, "/");

	REQUIRE(result.at(0) == "one");
	REQUIRE(result.at(1) == "two");
	REQUIRE(result.at(2) == "three");
	REQUIRE(result.at(3) == "four");
}

TEST_CASE("Split string2", "[Strings]") {
	std::string text = "/lamp/{pin_state}";


	std::vector<std::string> result = grestpio::string::split(text, "/");

	REQUIRE(result.at(0) == "lamp");
	REQUIRE(result.at(1) == "{pin_state}");
}

TEST_CASE("Split string3", "[Strings]") {
	std::string text = "///////";


	std::vector<std::string> result = grestpio::string::split(text, "/");

	REQUIRE(result.size() == 0);
}

TEST_CASE("Split string4", "[Strings]") {
	std::string text = "dasda";


	std::vector<std::string> result = grestpio::string::split(text, "/");

	REQUIRE(result.size() == 1);
	REQUIRE(result.at(0) == "dasda");
}

TEST_CASE("Split string5", "[Strings]") {
	std::string text = "/dasda/";


	std::vector<std::string> result = grestpio::string::split(text, "/");

	REQUIRE(result.size() == 1);
	REQUIRE(result.at(0) == "dasda");
}