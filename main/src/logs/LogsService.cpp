
#include <fstream>
#include <sstream>
#include "LogsService.hpp"


std::string grestpio::LogsService::get_logs() {
	std::fstream log_file;
	log_file.open("logs/grestpio.log", std::fstream::in);
	if (!log_file.is_open()) {
		return "Cannot read logs";
	}

	std::stringstream logs_content;
	while (!log_file.eof()) {
		std::string line;
		std::getline(log_file, line);
		logs_content << line << "\n";
	}

	return logs_content.str();
}
