#ifndef GRESTPIO_LOGSSERVICE_HPP
#define GRESTPIO_LOGSSERVICE_HPP

#include <string>

namespace grestpio {
	class LogsService final {
	public:
		static LogsService& instance() {
			static LogsService instance;
			return instance;
		}
		LogsService() = default;
		LogsService(const LogsService &) = delete;
		LogsService &operator=(const LogsService &) = delete;

		std::string get_logs();
	};
}

#endif //GRESTPIO_LOGSSERVICE_HPP
