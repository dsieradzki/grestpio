#ifndef GRESTPIO_SETTINGS_DTO_HPP
#define GRESTPIO_SETTINGS_DTO_HPP

#include <json.hpp>
#include <logger/Logger.hpp>

static std::map<dstools::logger::level, std::string> log_levels_obj_to_strings = {
		{dstools::logger::level::LOG_DEBUG,   "LOG_DEBUG"},
		{dstools::logger::level::LOG_INFO,    "LOG_INFO"},
		{dstools::logger::level::LOG_WARNING, "LOG_WARNING"},
		{dstools::logger::level::LOG_ERROR,   "LOG_ERROR"}
};

static std::map<std::string, dstools::logger::level> log_levels_strings_to_obj = {
		{"LOG_DEBUG",   dstools::logger::level::LOG_DEBUG},
		{"LOG_INFO",    dstools::logger::level::LOG_INFO},
		{"LOG_WARNING", dstools::logger::level::LOG_WARNING},
		{"LOG_ERROR",   dstools::logger::level::LOG_ERROR}
};

namespace grestpio {
	struct Settings {
		dstools::logger::level log_level;

		nlohmann::json serialize() const {
			return nlohmann::json{
					{"logLevel", log_levels_obj_to_strings[log_level]}
			};
		}

		static Settings deserialize(const std::string &value) {
			nlohmann::json json = nlohmann::json::parse(value);
			return {.log_level = log_levels_strings_to_obj[json["logLevel"]]};
		}
	};
}


#endif //GRESTPIO_SETTINGS_HPP
