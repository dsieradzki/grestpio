#ifndef GRESTPIO_SETTINGSSERVICE_HPP
#define GRESTPIO_SETTINGSSERVICE_HPP

#include "Settings.hpp"
#include <logger/LoggerProvider.hpp>

namespace grestpio {
    class SettingsService {
    public:
        static SettingsService &instance() {
            static SettingsService instance;
            return instance;
        }

        SettingsService() = default;
        virtual ~SettingsService() = default;
        SettingsService(const SettingsService &) = delete;
        SettingsService &operator=(const SettingsService &) = delete;


        void save_settings(const Settings &settings);
        Settings load_settings() const;
    };
}


#endif //GRESTPIO_SETTINGSSERVICE_HPP
