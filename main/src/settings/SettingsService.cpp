#include "SettingsService.hpp"
#include <fstream>
#include <sstream>

#define SETTINGS_FILE "conf/settings.json"

grestpio::Settings grestpio::SettingsService::load_settings() const {
    std::fstream settings_file;
    settings_file.open(SETTINGS_FILE);
    if (settings_file.fail()) {
        LOG_ERROR({ "Cannot open ", SETTINGS_FILE, " to read settings - ", strerror(errno) });
        throw std::runtime_error("Cannot open conf/settings.json");
    }

    std::stringstream ss;
    while (!settings_file.eof()) {
        std::string line;
        std::getline(settings_file, line);
        ss << line << "\n";
    }

    return grestpio::Settings::deserialize(ss.str());
}

void grestpio::SettingsService::save_settings(const grestpio::Settings &settings) {
    std::fstream settings_file;
    settings_file.open(SETTINGS_FILE, std::ios::out | std::ios::trunc);
    if (settings_file.fail()) {
        LOG_ERROR({ "Cannot open ", SETTINGS_FILE, " to save settings - ", strerror(errno) });
        throw std::runtime_error(std::string("Cannot open ") + SETTINGS_FILE + " to save settings");
    }

    std::string content = settings.serialize().dump();
    settings_file.write(content.c_str(), content.length());
    settings_file.close();
    LOG_INFO("Settings saved");
}
