
#ifndef GRESTPIO_GPIO_HPP
#define GRESTPIO_GPIO_HPP

#include "GpioChangeStateEvent.hpp"
#include <functional>
#include <memory>

namespace grestpio {
	namespace gpio {

		typedef std::string Pin;
		typedef bool State;

		class Gpio {
		public:
			Gpio() = default;
			virtual ~Gpio() = default;
			Gpio(const Gpio &) = delete;
			Gpio &operator=(const Gpio &) = delete;

			virtual void setup() = 0;
			virtual void shutdown() = 0;

			virtual void set_pit_state(Pin pin, State state) = 0;
			virtual void set_on_change_pin_state_listener(std::function<void(GpioChangeStateEvent)> listener) = 0;
		};

		extern std::shared_ptr<Gpio> create();
	}
}


#endif //GRESTPIO_GPIO_HPP
