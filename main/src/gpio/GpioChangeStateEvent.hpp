
#ifndef GRESTPIO_GPIOCHANGESTATEEVENT_HPP
#define GRESTPIO_GPIOCHANGESTATEEVENT_HPP



namespace grestpio {
	namespace gpio {
		typedef std::string Pin;
		typedef bool State;

		struct GpioChangeStateEvent {
			Pin pin;
			State state;
		};
	}
}


#endif //GRESTPIO_GPIOCHANGESTATEEVENT_HPP
