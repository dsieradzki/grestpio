
#ifndef GRESTPIO_SERIALIZATION_HPP
#define GRESTPIO_SERIALIZATION_HPP

#include <string>
#include <vector>
#include <json.hpp>
#include <logger/LoggerProvider.hpp>

namespace grestpio {
	namespace serialization {
		template <typename T>
		nlohmann::json serialize(std::vector<T> value) {
			nlohmann::json result = nlohmann::json::array();
			for (const T &e : value) {
				result.emplace_back(e.serialize());
			}
			return result;
		}

		template <typename T, typename C>
		C deserialize(nlohmann::json json) {
			C result;
			if (json.is_null()) {
				return result;
			}
			if (!json.is_array()) {
				LOG_ERROR("JSON have to be array");
				return result;
			}

			for (nlohmann::json::iterator it = json.begin(); it != json.end(); ++it) {
				result.emplace_back(T::deserialize(*it));
			}
			return result;
		}
	}
}


#endif //GRESTPIO_SERIALIZATION_HPP
