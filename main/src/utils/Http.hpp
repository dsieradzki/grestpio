
#ifndef GRESTPIO_HTTP_HPP
#define GRESTPIO_HTTP_HPP

#include <logger/LoggerProvider.hpp>

#define HTTP_OPERATION(x) \
    try {\
        x\
    } \
    catch(const std::runtime_error& re) {\
        LOG_ERROR({"HTTP Server Error - ", re.what()}); \
        res.status = 500;\
    }\
    catch(const std::exception& ex) {\
        LOG_ERROR({"HTTP Server Error - ", ex.what()}); \
        res.status = 500;\
    }\
    catch (...) {\
        LOG_ERROR("HTTP Server Error - unknown error occurred"); \
        res.status = 500;\
    }\

#endif //GRESTPIO_HTTP_HPP
