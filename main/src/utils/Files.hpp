
#ifndef GRESTPIO_FILES_HPP
#define GRESTPIO_FILES_HPP


#include <sys/stat.h>
#include <string>
#include <sstream>
#include <fstream>

namespace grestpio {
	namespace filesystem {
		bool is_exists(const std::string &path) {
			struct stat m_stat;
			return (stat(path.c_str(), &m_stat) == 0);
		}

		bool create_directory(const std::string &name) {
			return mkdir(name.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) != -1;
		}

		std::string read_all(std::fstream &f_stream) {
			std::stringstream ss;
			while (!f_stream.eof()) {
				std::string line;
				std::getline(f_stream, line);
				ss << line << "\n";
			}
			return ss.str();
		}
	}
}


#endif //GRESTPIO_FILES_HPP
