#ifndef GRESTPIO_STRING_HPP
#define GRESTPIO_STRING_HPP

#include <vector>
#include <string>

namespace grestpio {
	namespace string {
		std::vector<std::string> split(const std::string &value, const std::string &delimeter) {
			std::vector<std::string> result;

			std::size_t start_pos = 0;
			while (start_pos < value.length() - 1) {
				std::size_t index = value.find(delimeter, start_pos);
				if (index != std::string::npos) {
					if (start_pos != index) {
						result.emplace_back(value.substr(start_pos, index - start_pos));
					}
					start_pos = index + 1;
				} else {
					result.emplace_back(value.substr(start_pos, value.length()));
					start_pos = value.length();
				}
			}
			return result;
		}

		std::string replace(const std::string &input, const std::string &searched, const std::string &replacement) {
			std::string result(input);
			std::size_t position = result.find(searched);
			if (position != std::string::npos) {
				result.replace(position, searched.length(), replacement);
			}
			return result;
		}
	}
}


#endif //GRESTPIO_STRING_HPP
