
#ifndef GRESTPIO_DISPATCHER_HPP
#define GRESTPIO_DISPATCHER_HPP

#include <memory>
#include <server/iorest/IORestServer.hpp>
#include <gpio/Gpio.hpp>
#include <configuration/Configuration.hpp>


namespace grestpio {
	class Dispatcher final {
	public:
		Dispatcher(std::shared_ptr<gpio::Gpio> gpio, std::shared_ptr<IORestServer> io_rest_server);
		void setup();
	private:
		void on_gpio_change(gpio::GpioChangeStateEvent event);
		void on_rest_request(const httplib::Request &req, httplib::Response &res);

		std::shared_ptr<grestpio::gpio::Gpio> m_gpio;
		std::shared_ptr<grestpio::IORestServer> m_io_rest_server;
		bool is_url_match(const std::string &config_rule_url, const std::string &url);
		std::map<std::string, std::string> fetch_params_from_url(const std::string &config_rule_url, const std::string &url);
	};
}


#endif //GRESTPIO_DISPATCHER_HPP
