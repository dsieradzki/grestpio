#include <logger/LoggerProvider.hpp>
#include <utils/String.hpp>
#include <configuration/ConfigurationService.hpp>
#include "Dispatcher.hpp"

static const char *APPLICATION_JSON = "application/json";

grestpio::Dispatcher::Dispatcher(std::shared_ptr<grestpio::gpio::Gpio> gpio,
								 std::shared_ptr<grestpio::IORestServer> io_rest_server)
		: m_gpio(std::move(gpio)),
		  m_io_rest_server(std::move(io_rest_server)) {}

void grestpio::Dispatcher::setup() {
	LOG_INFO("Setup dispatcher");

	m_gpio->set_on_change_pin_state_listener(std::bind(&Dispatcher::on_gpio_change, this, std::placeholders::_1));
	m_io_rest_server->set_on_request_listener(std::bind(&Dispatcher::on_rest_request, this, std::placeholders::_1, std::placeholders::_2));
}


// CORE FUNCTIONALITY
void grestpio::Dispatcher::on_gpio_change(grestpio::gpio::GpioChangeStateEvent event) {
	static std::string pin_state_tag = "{pin_state}";

	LOG_INFO("Loading configuration");
	//TODO: Optimize not loading configuration every request
	grestpio::Configuration config = ConfigurationService::instance().load_configuration();

	for (const ConfigurationRule &config_rule : config.gpio_to_rest) {
		std::string pin = config_rule.input.at("pin");
		int port = std::stoi(config_rule.output.at("port"));
		std::string host = config_rule.output.at("host");
		std::string endpoint = config_rule.output.at("endpoint");
		std::string method = config_rule.output.at("method");
		std::string body;
		if (config_rule.output.find("body") != config_rule.output.end()) {
			body = config_rule.output.at("body");
		}

		if (pin == event.pin) {
			LOG_INFO({ "Found rule: [", config_rule.name, "]" });
			httplib::Client cli(host.c_str(), port, 5);

			endpoint = grestpio::string::replace(endpoint, pin_state_tag, event.state ? "1" : "0");
			body = grestpio::string::replace(body, pin_state_tag, event.state ? "1" : "0");

			std::shared_ptr<httplib::Response> res;

			if (method == "GET") res = cli.Get(endpoint.c_str());
			else if (method == "POST") res = cli.Post(endpoint.c_str(), body, APPLICATION_JSON);
			else if (method == "PUT") res = cli.Put(endpoint.c_str(), body, APPLICATION_JSON);
			else if (method == "DELETE") res = cli.Delete(endpoint.c_str());

			//TODO: Add HTTP Status check - problem with status from httplib
			if (res) {
				LOG_INFO({ "Response status ", std::to_string(res->status) });
			} else {
				LOG_ERROR({ "Cannot get response from: ", host, ":", std::to_string(port), endpoint });
			}
		}
	}
}


void grestpio::Dispatcher::on_rest_request(const httplib::Request &req, httplib::Response &res) {
	std::string path = req.path;
	LOG_INFO("Loading configuration");
	//TODO: Optimize not loading configuration every request
	grestpio::Configuration config = ConfigurationService::instance().load_configuration();

	for (const ConfigurationRule &config_rule : config.rest_to_gpio) {
		if (is_url_match(config_rule.input.at("url"), path)) {
			LOG_INFO({ "Found rule: [", config_rule.name, "]" });
			std::map<std::string, std::string> params = fetch_params_from_url(config_rule.input.at("url"), path);
			m_gpio->set_pit_state(config_rule.output.at("pin"), params.at("{pin_state}") != "0");
		}
	}
	res.set_content(path, "text/plain");
}

bool grestpio::Dispatcher::is_url_match(const std::string &config_rule_url, const std::string &url) {
	//TODO: REFACTOR THIS UGLY CODE
	const static std::regex find_placeholder("(\\{.*\\})");
	const static std::regex find_slashes("\\/");
	const static std::string replacement_regex = "\\w*"; // w* is this case is found text
	// Replace placeholders
	std::string url_pattern = std::regex_replace(config_rule_url, find_placeholder, replacement_regex);

	// Replace slashes
	const static std::string replacement_regex_for_slashes = "\\/";
	url_pattern = std::regex_replace(url_pattern, find_slashes, replacement_regex_for_slashes);

	std::regex result_regex(url_pattern);
	return std::regex_match(url, result_regex);
}

std::map<std::string, std::string> grestpio::Dispatcher::fetch_params_from_url(const std::string &config_rule_url, const std::string &url) {
	const static std::regex find_placeholder("(\\{)(.*)(\\})");

	std::vector<std::string> config_url_parts = grestpio::string::split(config_rule_url, "/");
	std::vector<std::string> url_parts = grestpio::string::split(url, "/");

	std::map<std::string, std::string> result;
	for (std::size_t i = 0; i < config_url_parts.size(); i++) {
		if (std::regex_match(config_url_parts.at(i), find_placeholder)) {
			result[config_url_parts.at(i)] = url_parts.at(i);
		}
	}
	return result;
}