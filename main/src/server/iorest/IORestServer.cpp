#include <utility>

#include <logger/LoggerProvider.hpp>
#include "IORestServer.hpp"
#include <utils/Http.hpp>

#define ANY_REQUEST_PATH R"(/.+)"

namespace grestpio {
	const char *iorest_host = "0.0.0.0";

	void IORestServer::setup(const StartupConfig &config) {
		LOG_INFO("Setup IO Rest server");
		this->m_config = config;
		m_server.Get("/", [](const httplib::Request &req, httplib::Response &res) {
			res.set_content("gRESTpIO - IO Rest server works", "text/plain");
		});

		m_server.Get(ANY_REQUEST_PATH, [&](const httplib::Request &req, httplib::Response &res) {
			HTTP_OPERATION(
					if (m_on_request_listener) {
						m_on_request_listener(req, res);
					} else {
						LOG_WARNING("IORestServer - received request but listener is not setup");
					}
			);
		});
	}

	void IORestServer::serve() {
		std::lock_guard<std::mutex> lock(m_serve_stop_mutex);
		if (m_server.is_running()) {
			LOG_ERROR("Server already started");
		}
		m_listen_thread = std::thread([=]() {
			LOG_INFO({ "IO Rest server started on port: ", std::to_string(m_config.io_rest_server_port) });
			m_server.listen(iorest_host, m_config.io_rest_server_port);
		});
		while (!m_server.is_running()) {
			std::this_thread::sleep_for(std::chrono::microseconds(100));
		}
	}

	void IORestServer::notify_shutdown() {
		std::lock_guard<std::mutex> lock(m_serve_stop_mutex);
		if (m_server.is_running()) {
			LOG_INFO("Notify IO Rest server to shutdown");
			m_server.stop();
		}
	}

	void IORestServer::wait_for_finish() {
		if (m_listen_thread.joinable()) {
			m_listen_thread.join();
		}
	}

	IORestServer::~IORestServer() {
		notify_shutdown();
		wait_for_finish();
	}

	void IORestServer::set_on_request_listener(std::function<void(const httplib::Request &, httplib::Response &)> on_request_listener) {
		this->m_on_request_listener = std::move(on_request_listener);
	}

}
