
#ifndef GRESTPIO_IORESTSERVER_HPP
#define GRESTPIO_IORESTSERVER_HPP

#include <httplib.h>
#include <application/StartupConfig.hpp>
#include <mutex>

namespace grestpio {
	class IORestServer final {
	public:
		IORestServer() = default;
		virtual ~IORestServer();
		IORestServer(const IORestServer &) = delete;
		IORestServer &operator=(const IORestServer &) = delete;

		void setup(const StartupConfig &config);
		void set_on_request_listener(std::function<void(const httplib::Request &, httplib::Response &)> on_request_listener);
		void serve();
		void notify_shutdown();
		void wait_for_finish();
	private:
		std::function<void(const httplib::Request &, httplib::Response &)> m_on_request_listener;
		StartupConfig m_config;
		httplib::Server m_server;
		std::thread m_listen_thread;
		std::mutex m_serve_stop_mutex;
	};

}

#endif
