
#include <logger/LoggerProvider.hpp>
#include "WebAppServer.hpp"


namespace grestpio {
	const char *webapp_host = "0.0.0.0";

	void WebAppServer::setup(const StartupConfig &config) {
		this->m_config = config;

		if (config.web_server_enabled) {
			setup_web_server();
		}
	}

	void WebAppServer::setup_web_server() {
		LOG_INFO("Setup Web server");
		m_configuration_rest_controller.setup(m_server);
		m_logs_rest_controller.setup(m_server);
		m_settings_rest_controller.setup(m_server);

		m_server.set_base_dir(m_config.web_file_path.c_str());
	}

	void WebAppServer::serve() {
		std::lock_guard<std::mutex> lock(m_serve_stop_mutex);
		if (m_server.is_running()) {
			LOG_ERROR("Server already started");
		}
		if (m_config.web_server_enabled) {
			m_listen_thread = std::thread([=]() {
				LOG_INFO({ "Web App Server started on port: ", std::to_string(m_config.web_server_port) });
				m_server.listen(webapp_host, m_config.web_server_port);
			});
			while (!m_server.is_running()) {
				std::this_thread::sleep_for(std::chrono::microseconds(100));
			}
		} else {
			LOG_INFO("Web App Server - DISABLED");
		}
	}

	void WebAppServer::notify_shutdown() {
		std::lock_guard<std::mutex> lock(m_serve_stop_mutex);
		if (m_server.is_running()) {
			LOG_INFO("Notify Web App Server to shutdown");
			m_server.stop();
		}
	}

	void WebAppServer::wait_for_finish() {
		if (m_listen_thread.joinable()) {
			m_listen_thread.join();
		}
	}

	WebAppServer::~WebAppServer() {
		notify_shutdown();
		wait_for_finish();
	}
}
