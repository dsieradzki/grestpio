
#ifndef GRESTPIO_WEBAPP_SERVER_HPP
#define GRESTPIO_WEBAPP_SERVER_HPP


#include <httplib.h>
#include <application/StartupConfig.hpp>
#include <server/webapp/controller/configuration/ConfigurationRestController.hpp>
#include <server/webapp/controller/logs/LogsRestController.hpp>
#include <server/webapp/controller/settings/SettingsRestController.hpp>

namespace grestpio {
	class WebAppServer final {
	public:
		WebAppServer() = default;
		~WebAppServer();
		WebAppServer(const WebAppServer &) = delete;
		WebAppServer &operator=(const WebAppServer &) = delete;
		void setup(const StartupConfig &config);
		void serve();
		void notify_shutdown();
		void wait_for_finish();
	private:
		StartupConfig m_config;
		httplib::Server m_server;
		std::thread m_listen_thread;
		std::mutex m_serve_stop_mutex;
		void setup_web_server();

		rest::controller::ConfigurationRestController m_configuration_rest_controller;
		rest::controller::LogsRestController m_logs_rest_controller;
		rest::controller::SettingsRestController m_settings_rest_controller;
	};
}

#endif
