
#include <logger/LoggerProvider.hpp>
#include <json.hpp>
#include <logs/LogsService.hpp>
#include "LogsRestController.hpp"


namespace grestpio {
	namespace rest {
		namespace controller {
			void LogsRestController::setup(httplib::Server &server) {
				server.Get("/api/logs", [=](const httplib::Request &req, httplib::Response &res) {
					LOG_INFO("Load logs");
					res.body = nlohmann::json{
							{"logs", LogsService::instance().get_logs()},
					}.dump();
				});
			}
		}
	}
}
