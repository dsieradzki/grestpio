#ifndef GRESTPIO_LOGSRESTCONTROLLER_HPP
#define GRESTPIO_LOGSRESTCONTROLLER_HPP

#include <httplib.h>

namespace grestpio {
	namespace rest {
		namespace controller {
			class LogsRestController {
			public:
				LogsRestController() = default;
				virtual ~LogsRestController() = default;

				LogsRestController(const LogsRestController &) = delete;
				LogsRestController &operator=(const LogsRestController &) = delete;

				void setup(httplib::Server &server);
			private:
			};
		}
	}
}


#endif //GRESTPIO_LOGSRESTCONTROLLER_HPP
