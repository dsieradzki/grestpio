#include <logger/LoggerProvider.hpp>
#include <metaconfiguration/MetaConfigurationService.hpp>
#include "ConfigurationRestController.hpp"
#include <utils/Http.hpp>

void grestpio::rest::controller::ConfigurationRestController::setup(httplib::Server &server) {
	server.Post("/api/configuration", [=](const httplib::Request &req, httplib::Response &res) {
		HTTP_OPERATION(
				LOG_INFO("Saving configuration");
				save_configuration(grestpio::Configuration::deserialize(req.body));
				LOG_INFO("Configuration saved");
		);
	});
	server.Get("/api/configuration", [=](const httplib::Request &req, httplib::Response &res) {
		HTTP_OPERATION(
				LOG_INFO("Loading configuration");
				res.body = load_configuration().serialize().dump();
				LOG_INFO("Configuration loaded");
		);
	});
	server.Get("/api/metaconfiguration", [=](const httplib::Request &req, httplib::Response &res) {
		HTTP_OPERATION(
				LOG_INFO("Loading meta configuration");
				res.body = load_metaconfiguration().serialize().dump();
				LOG_INFO("Meta configuration loaded");
		);
	});
}

void grestpio::rest::controller::ConfigurationRestController::save_configuration(grestpio::Configuration config) {
	ConfigurationService::instance().save_configuration(config);
}

grestpio::Configuration grestpio::rest::controller::ConfigurationRestController::load_configuration() {
	return ConfigurationService::instance().load_configuration();
}

grestpio::MetaConfiguration grestpio::rest::controller::ConfigurationRestController::load_metaconfiguration() {
	return MetaConfigurationService::instance().load_metaconfiguration();
}
