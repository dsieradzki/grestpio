#ifndef GRESTPIO_CONFIGURATIONRESTCONTROLLER_HPP
#define GRESTPIO_CONFIGURATIONRESTCONTROLLER_HPP


#include <httplib.h>
#include <configuration/Configuration.hpp>
#include <configuration/ConfigurationService.hpp>
#include <metaconfiguration/MetaConfiguration.hpp>

namespace grestpio {
	namespace rest {
		namespace controller {
			class ConfigurationRestController {
			public:
				ConfigurationRestController() = default;
				virtual ~ConfigurationRestController() = default;
				ConfigurationRestController(const ConfigurationRestController &) = delete;
				ConfigurationRestController &operator=(const ConfigurationRestController &) = delete;

				void setup(httplib::Server &server);
			private:
				void save_configuration(Configuration config);
				Configuration load_configuration();
				MetaConfiguration load_metaconfiguration();
			};
		}
	}
}


#endif //GRESTPIO_CONFIGURATIONRESTCONTROLLER_HPP
