#ifndef GRESTPIO_SETTINGSRESTCONTROLLER_HPP
#define GRESTPIO_SETTINGSRESTCONTROLLER_HPP


#include <httplib.h>
#include <logger/LoggerProvider.hpp>
#include <settings/Settings.hpp>

namespace grestpio {
	namespace rest {
		namespace controller {
			class SettingsRestController {
			public:
				SettingsRestController() = default;
				virtual ~SettingsRestController() = default;

				SettingsRestController(const SettingsRestController &) = delete;
				SettingsRestController &operator=(const SettingsRestController &) = delete;


				void setup(httplib::Server &server);

			private:
				Settings get_settings() const;
				void save_settings(const Settings &settings);
			};
		}
	}
}


#endif //GRESTPIO_SETTINGSRESTCONTROLLER_HPP
