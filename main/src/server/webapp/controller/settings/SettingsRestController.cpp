#include "SettingsRestController.hpp"
#include <json.hpp>
#include <utils/Http.hpp>
#include <settings/SettingsService.hpp>

void grestpio::rest::controller::SettingsRestController::setup(httplib::Server &server) {
    server.Get("/api/settings", [=](const httplib::Request &req, httplib::Response &res) {
        HTTP_OPERATION(
                LOG_INFO("Load logs");
                res.body = get_settings().serialize().dump();
        );
    });

    server.Post("/api/settings", [=](const httplib::Request &req, httplib::Response &res) {
        HTTP_OPERATION(
                save_settings(Settings::deserialize(req.body));
        );
    });


}

grestpio::Settings grestpio::rest::controller::SettingsRestController::get_settings() const {
    const Settings settings = SettingsService::instance().load_settings();
    LOG_INFO("Settings loaded");
    return settings;
}

void grestpio::rest::controller::SettingsRestController::save_settings(const grestpio::Settings &settings) {
    LOG_INFO({ "Log level changed to: ", log_levels_obj_to_strings[settings.log_level] });
    LOG_INFO("Settings saved");

    dstools::LoggerProvider::get_logger()->set_level(settings.log_level);
    SettingsService::instance().save_settings(settings);
}
