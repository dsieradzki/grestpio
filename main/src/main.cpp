#include <logger/LoggerProvider.hpp>
#include <arguments/Arguments.hpp>
#include <application/StartupConfig.hpp>
#include <application/Application.hpp>
#include <application/Constants.hpp>
#include <settings/SettingsService.hpp>
#include <configuration/ConfigurationService.hpp>
#include <csignal>
#include <utils/Files.hpp>


grestpio::Application application;

void on_signal(int signal) {
	LOG_WARNING({ "Shutdown application... ", "[signal: ", std::to_string(signal), "]" });
	application.notify_shutdown();
}

void setup_signals() {
	std::signal(SIGINT, on_signal);
	std::signal(SIGQUIT, on_signal);
	std::signal(SIGTERM, on_signal);
}

void create_app_directories_if_needed() {
	if (!grestpio::filesystem::is_exists("conf")) {
		if (!grestpio::filesystem::create_directory("conf")) {
			throw std::runtime_error("Cannot create /conf dir");
		}
	}
	if (!grestpio::filesystem::is_exists("logs")) {
		if (!grestpio::filesystem::create_directory("logs")) {
			throw std::runtime_error("Cannot create /logs dir");
		}
	}
}

void persist_empty_config_files_if_not_exists() {
	try {
		grestpio::ConfigurationService::instance().load_configuration();
	} catch (std::runtime_error &e) {
		LOG_WARNING("Save empty configuration");
		grestpio::ConfigurationService::instance().save_configuration(grestpio::Configuration());
	}

	try {
		grestpio::SettingsService::instance().load_settings();
	} catch (std::runtime_error &e) {
		LOG_WARNING("Save empty settings");
		grestpio::SettingsService::instance().save_settings(
				grestpio::Settings{.log_level=dstools::logger::level::LOG_WARNING});
	}
}

int main(int argc, char **argv) {

	setup_signals();
	create_app_directories_if_needed();
	persist_empty_config_files_if_not_exists();

	const grestpio::Settings settings = grestpio::SettingsService::instance().load_settings();
	dstools::LoggerProvider::set_level(settings.log_level);
	LOG_INFO("Setup logging level");

	std::vector<std::unique_ptr<dstools::logger::Appender>> appenders;
	appenders.emplace_back(std::unique_ptr<dstools::logger::ConsoleAppender>(new dstools::logger::ConsoleAppender()));
	appenders.emplace_back(std::unique_ptr<dstools::logger::FileAppender>(
			new dstools::logger::FileAppender(grestpio::files::LOG_FILE)));

	dstools::LoggerProvider::setup_logger(
			std::unique_ptr<dstools::logger::DateTimeFormatter>(new dstools::logger::DateTimeFormatter()),
			std::move(appenders));

	dstools::Arguments arguments(argc, argv);
	const grestpio::StartupConfig config{
			.web_server_enabled  =          !arguments.is_present(grestpio::web::DISABLE_WEB_APP),
			.web_server_port     = std::stoi(arguments.argument_value(grestpio::web::WEBAPP_SERVER_PORT, std::to_string(
					grestpio::web::WEP_APP_DEFAULT_SERVER_PORT))),
			.web_file_path       =           arguments.argument_value(grestpio::web::WEB_APP_PATH,
																	  grestpio::web::DEFAULT_WEB_APP_PATH),
			.io_rest_server_port = std::stoi(arguments.argument_value(grestpio::web::IO_REST_SERVER_PORT,
																	  std::to_string(
																			  grestpio::web::IO_REST_DEFAULT_SERVER_PORT)))
	};


	if (application.init(config)) {
		application.serve();
	}
}