#ifndef GRESTPIO_CONSTANTS_HPP
#define GRESTPIO_CONSTANTS_HPP

namespace grestpio {
	namespace web {
		const char *DISABLE_WEB_APP = "--disable-webapp";
		const char *WEB_APP_PATH = "--webapp-path";
		const char *DEFAULT_WEB_APP_PATH = "./www";
		const char *WEBAPP_SERVER_PORT = "--webapp-port";
		const char *IO_REST_SERVER_PORT = "--io-port";
		const int WEP_APP_DEFAULT_SERVER_PORT = 9080;
		const int IO_REST_DEFAULT_SERVER_PORT = 9090;
	}

	namespace files {
		const char *LOG_FILE = "logs/grestpio.log";
		const char *RULES_FILE = "conf/rules.json";
	}
}


#endif //GRESTPIO_CONSTANTS_HPP
