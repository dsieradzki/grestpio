#ifndef GRESTPIO_APPLICATION_HPP
#define GRESTPIO_APPLICATION_HPP


#include <gpio/Gpio.hpp>
#include <server/webapp/WebAppServer.hpp>
#include <server/iorest/IORestServer.hpp>
#include <application/StartupConfig.hpp>
#include <configuration/Configuration.hpp>
#include <configuration/ConfigurationService.hpp>
#include <server/iorest/dispatcher/Dispatcher.hpp>

namespace grestpio {

	class Application final {
	public:
		Application();
		virtual ~Application();
		Application(const Application &) = delete;
		Application &operator=(const Application &) = delete;

		bool init(const StartupConfig &startup_config);
		void serve();
		void notify_shutdown();
	private:
		std::shared_ptr<grestpio::gpio::Gpio> m_gpio;
		std::shared_ptr<grestpio::IORestServer> m_io_rest_server;

		grestpio::Dispatcher m_dispatcher;
		grestpio::WebAppServer m_web_server;
	};
}


#endif //GRESTPIO_APPLICATION_HPP
