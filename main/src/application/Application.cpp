#include <utility>
#include <logger/LoggerProvider.hpp>
#include <settings/SettingsService.hpp>
#include "Application.hpp"

grestpio::Application::Application() :
        m_gpio(gpio::create()),
        m_io_rest_server(std::make_shared<grestpio::IORestServer>()),
        m_dispatcher(m_gpio, m_io_rest_server) {
}

grestpio::Application::~Application() {
    notify_shutdown();
}

bool grestpio::Application::init(const StartupConfig &startup_config) {
    m_gpio->setup();
    m_web_server.setup(startup_config);
    m_io_rest_server->setup(startup_config);
    m_dispatcher.setup();

    return true;
}

void grestpio::Application::serve() {
    m_web_server.serve();
    m_io_rest_server->serve();

    m_web_server.wait_for_finish();
    m_io_rest_server->wait_for_finish();
}

void grestpio::Application::notify_shutdown() {
    m_web_server.notify_shutdown();
    m_io_rest_server->notify_shutdown();
    m_gpio->shutdown();
}

