
#ifndef GRESTPIO_STARTCONFIG_HPP
#define GRESTPIO_STARTCONFIG_HPP


#include <string>

namespace grestpio {
	struct StartupConfig {
		bool        web_server_enabled;
		int         web_server_port;
		std::string web_file_path;

		int         io_rest_server_port;
	};
}


#endif //GRESTPIO_STARTCONFIG_HPP
