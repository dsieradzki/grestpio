
#include <sstream>
#include "ConfigurationService.hpp"

#define CONFIG_FILE "conf/rules.json"

grestpio::Configuration grestpio::ConfigurationService::load_configuration() const noexcept(false) {
    std::fstream config_file;
    config_file.open(CONFIG_FILE);
    if (config_file.fail()) {
        LOG_ERROR({ "Cannot open ", CONFIG_FILE, " to read rules - ", strerror(errno) });
        throw std::runtime_error(std::string("Cannot open ") + CONFIG_FILE);
    }

    std::stringstream ss;
    while (!config_file.eof()) {
        std::string line;
        std::getline(config_file, line);
        ss << line << "\n";
    }

    return Configuration::deserialize(ss.str());
}

void grestpio::ConfigurationService::save_configuration(const grestpio::Configuration &config) noexcept(false) {

    std::fstream config_file;
    config_file.open(CONFIG_FILE, std::ios::out | std::ios::trunc);
    if (config_file.fail()) {
        LOG_ERROR({ "Cannot open ", CONFIG_FILE, " to save rules - ", strerror(errno) });
        throw std::runtime_error(std::string("Cannot open ") + CONFIG_FILE + " to save configuration");
    }
    std::string content = config.serialize().dump(4);
    config_file.write(content.c_str(), content.length());
    config_file.close();
    LOG_INFO("Configuration persisted");
}
