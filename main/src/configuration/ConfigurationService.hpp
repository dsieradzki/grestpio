
#ifndef GRESTPIO_CONFIGURATION_HPP
#define GRESTPIO_CONFIGURATION_HPP

#include "Configuration.hpp"

namespace grestpio {
	class ConfigurationService final {
	public:
		static ConfigurationService &instance() {
			static ConfigurationService instance;
			return instance;
		}

		ConfigurationService() = default;
		virtual ~ConfigurationService() = default;
		ConfigurationService(const ConfigurationService &) = delete;
		ConfigurationService &operator=(const ConfigurationService &) = delete;

		Configuration load_configuration() const noexcept(false);
		void save_configuration(const Configuration &config) noexcept(false);
	private:
	};
}

#endif //GRESTPIO_CONFIGURATION_HPP
