
#ifndef GRESTPIO_CONFIG_HPP
#define GRESTPIO_CONFIG_HPP

#include <vector>
#include <json.hpp>
#include <utils/Serialization.hpp>
#include "ConfigurationRule.hpp"
#include <logger/LoggerProvider.hpp>

namespace grestpio {
	struct Configuration {
		std::vector<ConfigurationRule> gpio_to_rest;
		std::vector<ConfigurationRule> rest_to_gpio;

		nlohmann::json serialize() const {
			return nlohmann::json{
					{"gpio_to_rest", grestpio::serialization::serialize(gpio_to_rest)},
					{"rest_to_gpio", grestpio::serialization::serialize(rest_to_gpio)}
			};
		}

		static Configuration deserialize(const std::string &value) {
			nlohmann::json json = nlohmann::json::parse(value);
			return Configuration{
					.gpio_to_rest = grestpio::serialization::deserialize<ConfigurationRule, std::vector<ConfigurationRule>>(json["gpio_to_rest"]),
					.rest_to_gpio = grestpio::serialization::deserialize<ConfigurationRule, std::vector<ConfigurationRule>>(json["rest_to_gpio"])
			};
		}
	};
}


#endif //GRESTPIO_CONFIG_HPP
