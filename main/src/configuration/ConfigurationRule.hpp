
#ifndef GRESTPIO_CONFIGRULE_HPP
#define GRESTPIO_CONFIGRULE_HPP

#include <map>
#include <string>

namespace grestpio {
	struct ConfigurationRule {
		std::string name;
		std::map<std::string, std::string> input;
		std::map<std::string, std::string> output;

		nlohmann::json serialize() const {
			return nlohmann::json{
					{"name",   name},
					{"input",  input},
					{"output", output},
			};
		}

		static ConfigurationRule deserialize(nlohmann::json json) {
			return ConfigurationRule{
					.name = json.at("name").get<std::string>(),
					.input = json.at("input").get<std::map<std::string, std::string>>(),
					.output = json.at("output").get<std::map<std::string, std::string>>()
			};
		}
	};
}


#endif //GRESTPIO_CONFIGRULE_HPP
