
#include "MetaConfigurationService.hpp"

grestpio::MetaConfiguration grestpio::MetaConfigurationService::load_metaconfiguration() {
	return {
			.gpio_pins = {"1", "2", "3", "4", "5", "6"}
	};
}
