#ifndef GRESTPIO_METACONFIGURATION_HPP
#define GRESTPIO_METACONFIGURATION_HPP

#include <vector>
#include <string>
#include <json.hpp>
#include <utils/Serialization.hpp>


namespace grestpio {
	struct MetaConfiguration {
		std::vector<std::string> gpio_pins;


		nlohmann::json serialize() const {
			return nlohmann::json{
					{"gpio_pins", gpio_pins},
			};
		}

		static MetaConfiguration deserialize(const std::string &value) {
			nlohmann::json json = nlohmann::json::parse(value);
			return MetaConfiguration{
					.gpio_pins = json.get<std::vector<std::string>>()
			};
		}
	};
}


#endif //GRESTPIO_METACONFIGURATION_HPP
