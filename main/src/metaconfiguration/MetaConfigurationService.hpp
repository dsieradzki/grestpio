#ifndef GRESTPIO_METACONFIGURATIONSERVICE_HPP
#define GRESTPIO_METACONFIGURATIONSERVICE_HPP


#include "MetaConfiguration.hpp"

namespace grestpio {
	class MetaConfigurationService {
	public:
		static MetaConfigurationService &instance() {
			static MetaConfigurationService instance;
			return instance;
		}

		MetaConfigurationService() = default;
		virtual ~MetaConfigurationService() = default;
		MetaConfigurationService(const MetaConfigurationService &) = delete;
		MetaConfigurationService &operator=(const MetaConfigurationService &) = default;

		MetaConfiguration load_metaconfiguration();
	};
}


#endif //GRESTPIO_METACONFIGURATIONSERVICE_HPP
