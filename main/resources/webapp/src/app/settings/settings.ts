export class Settings {

  constructor(logLevel: string) {
    this.logLevel = logLevel;
  }

  /**
   * Expected values:
   *
   * LOG_DEBUG,
   * LOG_INFO,
   * LOG_WARNING,
   * LOG_ERROR
   */
  logLevel: string;
}
