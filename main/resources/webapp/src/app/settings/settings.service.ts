import {Injectable} from "@angular/core";
import {Settings} from "./settings";
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";


@Injectable({
  providedIn: "root"
})
export class SettingsService {

  constructor(private http: HttpClient) {

  }

  public saveSettings(settings: Settings): Observable<{}> {
    return this.http.post("/api/settings", settings);
  }

  public loadSettings(): Observable<Settings> {
    return this.http.get<Settings>("api/settings");
  }
}
