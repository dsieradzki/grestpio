import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {SettingsService} from "./settings.service";
import {Settings} from "./settings";
import {MatSnackBar} from "@angular/material";

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {
  private settingsForm: FormGroup;
  private logLevels: string[];

  constructor(private formBuilder: FormBuilder,
              private settingService: SettingsService,
              private snackBar: MatSnackBar) {
    this.settingsForm = this.formBuilder.group({
      logLevel: ["", Validators.required]
    });

    this.logLevels = ["LOG_DEBUG", "LOG_INFO", "LOG_WARNING", "LOG_ERROR"];
  }

  ngOnInit() {
    this.settingService.loadSettings()
      .subscribe((settings: Settings) => {
        this.settingsForm.controls["logLevel"].setValue(settings.logLevel);
      })
  }

  private onSave() {
    if (this.settingsForm.valid) {
      this.settingService.saveSettings(new Settings(
        this.settingsForm.controls["logLevel"].value as string
      ))
        .subscribe(() => {
            this.snackBar.open("Settings saved.");
            console.debug("Settings saved.");
          },
          (error) => {
            this.snackBar.open("Cannot save settings.");
            console.error("Cannot save settings.", error);
          });
    }
  }

}
