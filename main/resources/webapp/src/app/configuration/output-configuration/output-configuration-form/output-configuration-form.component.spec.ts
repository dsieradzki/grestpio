import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutputConfigurationFormComponent } from './output-configuration-form.component';

describe('OutputConfigurationFormComponent', () => {
  let component: OutputConfigurationFormComponent;
  let fixture: ComponentFixture<OutputConfigurationFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutputConfigurationFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutputConfigurationFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
