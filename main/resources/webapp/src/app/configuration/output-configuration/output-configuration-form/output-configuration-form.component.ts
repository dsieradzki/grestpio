import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ConfigurationRule} from "../../services/configuration/configuration";
import {MetaConfiguration} from "../../services/metaconfiguration/meta-configuration";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MatSelectChange} from "@angular/material";


@Component({
  selector: 'app-output-configuration-form',
  templateUrl: './output-configuration-form.component.html',
  styleUrls: ['./output-configuration-form.component.css']
})
export class OutputConfigurationFormComponent implements OnInit {
  private form: FormGroup;
  gpioPins: string[];
  methods: string[];
  private isEditEnabled: boolean = false;

  private initName: string;
  private initPin: string;
  private initHost: string;
  private initPort: string;
  private initEndpoint: string;
  private initMethod: string;
  private initBody: string;

  @Output() private onSaveClick: EventEmitter<ConfigurationRule> = new EventEmitter();
  @Output() private onDeleteClick: EventEmitter<string> = new EventEmitter();

  constructor(private formBuilder: FormBuilder) {
    this.form = this.formBuilder.group({
      name: ["", Validators.required],
      pin: ["", Validators.required],
      host: ["", Validators.required],
      port: ["", Validators.required],
      endpoint: ["", Validators.required],
      method: ["", Validators.required],
      body: ["", Validators.required]
    });
  }

  ngOnInit() {
    this.methods = [
      "GET",
      "POST",
      "PUT",
      "DELETE"
    ];
    this.form.controls["body"].disable();
  }

  @Input()
  set configuration(item: ConfigurationRule) {
    if (item == null || item.name == null || item.name == "") {
      this.form.controls["name"].setValue("");
      this.form.controls["pin"].setValue("");
      this.form.controls["host"].setValue("");
      this.form.controls["port"].setValue("");
      this.form.controls["endpoint"].setValue("");
      this.form.controls["method"].setValue("");
      this.form.controls["body"].setValue("");
      this.backupFormData();
      this.enableComponents(true);
    } else {
      this.form.controls["name"].setValue(item.name);
      this.form.controls["pin"].setValue(item.input["pin"] || "");
      this.form.controls["host"].setValue(item.output["host"] || "");
      this.form.controls["port"].setValue(item.output["port"] || "");
      this.form.controls["endpoint"].setValue(item.output["endpoint"] || "");
      this.form.controls["method"].setValue(item.output["method"] || "");
      this.form.controls["body"].setValue(item.output["body"] || "");
      this.backupFormData();
      this.enableComponents(false);
    }
  }

  @Input()
  set metaConfiguration(metaConf: MetaConfiguration) {
    if (metaConf != null) {
      this.gpioPins = metaConf.gpio_pins;
    }
  }

  private onSave() {
    if (this.form.valid) {
      this.backupFormData();
      this.enableComponents(false);

      this.onSaveClick.emit(new ConfigurationRule(
        this.form.controls["name"].value,
        {
          pin: "" + this.form.controls["pin"].value
        },
        {
          host: this.form.controls["host"].value,
          port: "" + this.form.controls["port"].value,
          endpoint: this.form.controls["endpoint"].value,
          method: "" + this.form.controls["method"].value,
          body: this.form.controls["body"].value
        }
      ));
    }
  }

  private onEdit() {
    this.enableComponents(true);
    this.backupFormData();
  }

  private onCancel() {
    this.enableComponents(false);
    this.restoreFormData();
  }

  private onDelete() {
    if (confirm("Are you sure?")) {
      this.onDeleteClick.emit(this.initName);
    }
  }

  private enableComponents(isEnable: boolean) {
    this.isEditEnabled = isEnable;
    const action = isEnable ? 'enable' : 'disable';
    this.form.controls["name"][action]();
    this.form.controls["pin"][action]();
    this.form.controls["host"][action]();
    this.form.controls["port"][action]();
    this.form.controls["endpoint"][action]();
    this.form.controls["method"][action]();
    if (OutputConfigurationFormComponent.canBodyBeEnable(this.form.controls["method"].value)) {
      this.form.controls["body"][action]();
    }
  }

  private backupFormData() {
    this.initName = this.form.controls["name"].value;
    this.initPin = this.form.controls["pin"].value;
    this.initHost = this.form.controls["host"].value;
    this.initPort = this.form.controls["port"].value;
    this.initEndpoint = this.form.controls["endpoint"].value;
    this.initMethod = this.form.controls["method"].value;
    this.initBody = this.form.controls["body"].value;
  }

  private restoreFormData() {
    this.form.controls["name"].setValue(this.initName);
    this.form.controls["pin"].setValue(this.initPin);
    this.form.controls["host"].setValue(this.initHost);
    this.form.controls["port"].setValue(this.initPort);
    this.form.controls["endpoint"].setValue(this.initEndpoint);
    this.form.controls["method"].setValue(this.initMethod);
    this.form.controls["body"].setValue(this.initBody);
  }

  private canBeCanceled(): boolean {
    return this.isEditEnabled && this.initName != null && this.initName != "";
  }

  private canBeDeleted(): boolean {
    return !this.isEditEnabled && this.initName != null && this.initName != "";
  }

  private static canBodyBeEnable(value: string): boolean {
    return (value == "POST" || value == "PUT");
  }

  private onHttpMethodChange(selection: MatSelectChange) {
    if (OutputConfigurationFormComponent.canBodyBeEnable(selection.value)) {
      this.form.controls["body"].enable();
    } else {
      this.form.controls["body"].disable();
      this.form.controls["body"].setValue("");
    }
  }

}
