import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutputConfigurationComponent } from './output-configuration.component';

describe('OutputConfigurationComponent', () => {
  let component: OutputConfigurationComponent;
  let fixture: ComponentFixture<OutputConfigurationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutputConfigurationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutputConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
