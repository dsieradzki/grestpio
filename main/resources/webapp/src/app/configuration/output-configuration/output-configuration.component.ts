import {Component, OnInit, ViewChild} from '@angular/core';
import {ConfigurationService} from "../services/configuration/configuration.service";
import {MetaConfigurationService} from "../services/metaconfiguration/meta-configuration.service";
import {Configuration, ConfigurationRule} from "../services/configuration/configuration";
import {MetaConfiguration} from "../services/metaconfiguration/meta-configuration";
import {ConfigurationListComponent} from "../configuration-list/configuration-list.component";

@Component({
  selector: 'app-output-configuration',
  templateUrl: './output-configuration.component.html',
  styleUrls: ['./output-configuration.component.css']
})
export class OutputConfigurationComponent implements OnInit {
  @ViewChild(ConfigurationListComponent) configurationListComponent: ConfigurationListComponent;

  metaConfiguration: MetaConfiguration;
  configuration: Configuration = new Configuration();
  selectedConfigurationRule?: ConfigurationRule;
  orginalSelectedRuleName?: string;

  constructor(private configurationService: ConfigurationService,
              private metaConfigurationService: MetaConfigurationService) {
  }

  onAddConfiguration() {
    this.selectedConfigurationRule = new ConfigurationRule(null, {}, {});
    this.orginalSelectedRuleName = null;
    this.configurationListComponent.deselect();
  }

  onSaveConfiguration(configToSave: ConfigurationRule) {
    this.updateConfig(configToSave);
    this.configurationService.saveConfiguration(this.configuration)
      .subscribe(() => {
        console.log("Configuration saved");
      });
    this.onSelectConfiguration(configToSave);
    this.configurationListComponent.select(configToSave.name);
  }

  onDeleteConfiguration(configNameToDelete: string) {
    for (let i = 0; i < this.configuration.gpio_to_rest.length; i++) {
      if (this.configuration.gpio_to_rest[i].name == configNameToDelete) {
        this.configuration.gpio_to_rest.splice(i, 1);
      }
    }
    this.configurationService.saveConfiguration(this.configuration)
      .subscribe(() => {
        console.log("Configuration saved");
        this.selectedConfigurationRule = null;
        this.orginalSelectedRuleName = null;
        this.configurationListComponent.deselect();
      })
  }

  private isFormVisible(): boolean {
    return this.selectedConfigurationRule != null;
  }

  private updateConfig(configToSave: ConfigurationRule) {
    var isNew = true;
    for (let i = 0; i < this.configuration.gpio_to_rest.length; i++) {
      if (this.configuration.gpio_to_rest[i].name == this.orginalSelectedRuleName) {
        isNew = false;
        this.configuration.gpio_to_rest[i].name = configToSave.name;
        this.configuration.gpio_to_rest[i].input = configToSave.input;
        this.configuration.gpio_to_rest[i].output = configToSave.output;
      }
    }

    if (isNew) {
      this.configuration.gpio_to_rest.push(configToSave);
    }
  }

  onSelectConfiguration(config: ConfigurationRule) {
    this.selectedConfigurationRule = config;
    this.orginalSelectedRuleName = config.name;
  }

  ngOnInit(): void {
    this.configurationService.loadConfiguration()
      .subscribe((resposne: Configuration) => {
        console.log("Configuration loaded");
        this.configuration.gpio_to_rest = resposne.gpio_to_rest;
        this.configuration.rest_to_gpio = resposne.rest_to_gpio;
      });

    this.metaConfigurationService.loadConfiguration()
      .subscribe((response) => {
        this.metaConfiguration = response;
        console.log("Meta configuration loaded");
      })
  }

}
