import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Configuration} from "./configuration"
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ConfigurationService {

  constructor(private http: HttpClient) {

  }

  saveConfiguration(config: Configuration): Observable<{}> {
    return this.http.post("/api/configuration", config);
  }

  loadConfiguration(): Observable<Configuration> {
    return this.http.get<Configuration>("/api/configuration");
  }
}
