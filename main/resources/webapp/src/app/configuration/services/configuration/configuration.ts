export class ConfigurationRule {

  constructor(name: string, input: Object, output: Object) {
    this.name = name;
    this.input = input;
    this.output = output;
  }

  name: string;
  input: Object;
  output: Object;
}

export class Configuration {
  gpio_to_rest: ConfigurationRule[];
  rest_to_gpio: ConfigurationRule[];
}
