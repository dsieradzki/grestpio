import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {MetaConfiguration} from "./meta-configuration";

@Injectable({
  providedIn: 'root'
})
export class MetaConfigurationService {

  constructor(private http: HttpClient) {

  }

  loadConfiguration(): Observable<MetaConfiguration> {
    return this.http.get<MetaConfiguration>("/api/metaconfiguration");
  }
}
