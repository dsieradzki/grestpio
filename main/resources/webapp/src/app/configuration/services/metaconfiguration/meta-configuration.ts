export class MetaConfiguration {

  constructor(gpio_pins: string[]) {
    this.gpio_pins = gpio_pins;
  }

  gpio_pins: string[];
}
