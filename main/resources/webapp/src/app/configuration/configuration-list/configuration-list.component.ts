import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ConfigurationRule} from "../services/configuration/configuration";

@Component({
  selector: 'app-configuration-list',
  templateUrl: './configuration-list.component.html',
  styleUrls: ['./configuration-list.component.css']
})
export class ConfigurationListComponent implements OnInit {

  @Input() private configurations: ConfigurationRule[];
  @Output() private onSelect: EventEmitter<ConfigurationRule> = new EventEmitter();

  private selected: ConfigurationRule;

  constructor() {
  }

  ngOnInit() {
  }

  public select(name: string) {
    for (let i = 0; i < this.configurations.length; i++) {
      if (this.configurations[i].name == name) {
        this.selected = this.configurations[i];
        break;
      }
    }
  }

  public deselect() {
    this.selected = null;
  }

  private isSelected(item: ConfigurationRule): boolean {
    return this.selected == item;
  }

  private onSelectConfiguration(item: ConfigurationRule) {
    this.selected = item;
    this.onSelect.emit(item);
  }
}
