import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Logs} from "./logs";

@Injectable({
  providedIn: 'root'
})
export class LogsService {

  constructor(private http: HttpClient) {

  }

  loadLogs(): Observable<Logs> {
    return this.http.get<Logs>("/api/logs");
  }
}
