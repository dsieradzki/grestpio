import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {LogsService} from "./logs.service";

@Component({
  selector: 'app-logs',
  templateUrl: './logs.component.html',
  styleUrls: ['./logs.component.css']
})
export class LogsComponent implements OnInit {

  logs: string;

  constructor(private logsService: LogsService) {

  }

  ngOnInit() {
    this.logsService.loadLogs()
      .subscribe((response) => {
        this.logs = response.logs;
        console.log("Logs loaded");
      })
  }

}
