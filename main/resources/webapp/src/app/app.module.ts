import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MaterialModule} from './material.module';
import {HttpClientModule} from "@angular/common/http";
import {FlexLayoutModule} from "@angular/flex-layout";
import {OutputConfigurationComponent} from './configuration/output-configuration/output-configuration.component';
import {ConfigurationListComponent} from './configuration/configuration-list/configuration-list.component';
import {LeftMenuItemComponent} from './left-menu/left-menu-item.component';
import {InputConfigurationComponent} from './configuration/input-configuration/input-configuration.component';
import {OutputConfigurationFormComponent} from './configuration/output-configuration/output-configuration-form/output-configuration-form.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { LogsComponent } from './logs/logs.component';
import { SettingsComponent } from './settings/settings.component';

@NgModule({
  declarations: [
    AppComponent,
    OutputConfigurationComponent,
    ConfigurationListComponent,
    LeftMenuItemComponent,
    InputConfigurationComponent,
    OutputConfigurationFormComponent,
    LogsComponent,
    SettingsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
