import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {OutputConfigurationComponent} from "./configuration/output-configuration/output-configuration.component";
import {InputConfigurationComponent} from "./configuration/input-configuration/input-configuration.component";
import {LogsComponent} from "./logs/logs.component";
import {SettingsComponent} from "./settings/settings.component";

const routes: Routes = [
  {path: 'output', component: OutputConfigurationComponent},
  {path: 'input', component: InputConfigurationComponent},
  {path: 'logs', component: LogsComponent},
  {path: 'settings', component: SettingsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
