import {Component, Input, OnInit} from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-left-menu-item',
  templateUrl: './left-menu-item.component.html',
  styleUrls: ['./left-menu-item.component.css']
})
export class LeftMenuItemComponent implements OnInit {

  @Input() name: string;
  @Input() topName: string;
  @Input() iconName: string;
  @Input() routerLink: string;

  constructor(private router : Router) {

  }

  isSelected(): boolean {
    return this.router.url === this.routerLink;
  }

  ngOnInit() {
  }

}
