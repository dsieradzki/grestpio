
#include <iostream>
#include <logger/LoggerProvider.hpp>

#include "GpioImpl.hpp"

grestpio::gpio::GpioImpl::GpioImpl() {

}

grestpio::gpio::GpioImpl::~GpioImpl() {

}


void grestpio::gpio::GpioImpl::setup() {
	m_server.Get(R"(/gpio/pin/(\d+)/state/(\d+))", [=](const httplib::Request &req, httplib::Response &res) {
		std::string pin = req.matches[1];
		std::string state = req.matches[2];
		res.set_content("Pin: " + pin + " changed to: " + state, "text/plain");
		set_pit_state(pin, state == "0" ? false : true);
	});

	m_server_thread = std::thread([=]() {
		LOG_INFO({ "MOCK GPIO started on port: ", std::to_string(9999) });
		m_server.listen("localhost", 9999);
	});
	while (!m_server.is_running()) {
		std::this_thread::sleep_for(std::chrono::microseconds(100));
	}
}

void grestpio::gpio::GpioImpl::shutdown() {
	LOG_INFO("Shutting down GPIO MOCK");
	m_server.stop();
	if (m_server_thread.joinable()) {
		m_server_thread.join();
	}
}

void grestpio::gpio::GpioImpl::set_pit_state(Pin pin, State state) {
	LOG_INFO({ "MOCK STATE CHANGE -> Pin[", pin, "] State:[", std::to_string(state), "]" });
	m_pins[pin] = state;
	if (m_listener) {
		m_listener(GpioChangeStateEvent{
				.pin=pin,
				.state = state
		});
	}
}

void grestpio::gpio::GpioImpl::set_on_change_pin_state_listener(std::function<void(GpioChangeStateEvent)> listener) {
	this->m_listener = listener;
}
