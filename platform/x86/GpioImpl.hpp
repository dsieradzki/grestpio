
#ifndef GRESTPIO_GPIOIMPL_HPP
#define GRESTPIO_GPIOIMPL_HPP

#include <gpio/Gpio.hpp>
#include <httplib.h>

namespace grestpio {
	namespace gpio {
		class GpioImpl : public Gpio {
		public:
			GpioImpl();
			~GpioImpl() override;

			void setup() override;
			void shutdown() override;


			void set_pit_state(Pin pin, State state) override;
			void set_on_change_pin_state_listener(std::function<void(GpioChangeStateEvent)> listener) override;
		private:
			httplib::Server m_server;
			std::thread m_server_thread;
			std::map<Pin, bool> m_pins;
			std::function<void(GpioChangeStateEvent)> m_listener;
		};

		std::shared_ptr<Gpio> create() {
			return std::make_shared<GpioImpl>();
		}
	}
}


#endif //GRESTPIO_GPIO_HPP
